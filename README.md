![Armando Schmitt Logo](https://cdn.plentymarkets.eu/item/images/5912/secondPreview/5912.png)

# Feed4Ceres for Ceres Shopbuilder

This is the **Feed4Ceres** widget developed by PIXELPUNKT. Use this plugin-widget to display a section with Image and Text in the Shopbuilder for [Ceres](https://github.com/plentymarkets/plugin-ceres) template plugin.

## Requirements

This is a widget plugin for [plentymarkets 7](https://www.plentymarkets.com). A layout plugin is required in order to integrate this plugin in your online store. We recommend using the plentymarkets plugin [Ceres](https://github.com/plentymarkets/plugin-ceres).

## Join the Plentymarkets community

Sign up today and become a member of our [forum](https://forum.plentymarkets.com/c/plugin-entwicklung). Discuss the latest trends in plugin development and share your ideas with our community.

## Versioning

Visit our forum and find the latest news and updates in our [Changelog](https://forum.plentymarkets.com/c/changelog?order=created).

## License

This project is licensed under the MIT LICENSE - see the [LICENSE.md](/LICENSE.md) file for details.

## Contact

We provide support over the Plentymarkets Forum. Please send us a PN or Tag us under @armando-21429 or send us a PN via Mail to info@armandoschmitt.de