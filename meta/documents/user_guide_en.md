# Feed4Ceres for Shop-Builder
    
This plugin adds a section with your Instagram Feed to the shop builder. The properties of the widget can be set globaly under your plugin sets. It is also possible to change the number of columns showing in the feed4ceres widget frontend of your shop. 

    
## Example 3-Columns Layout

<iframe src="https://spark.adobe.com/video/2ojgRe0hyYfqk"  width="100%" height="512" frameborder="0" allowfullscreen></iframe>

## Requirements
<div class="alert alert-info" role="alert"> 
:rotating_light: You need to have installed Ceres-Shop and the IO Plugin as base. The Shop Builder must also be activated. :bangbang:
</div>

## Installation
    
Download the plugin widget from the plentyMarketplace. In your Plentymarkets backend you can install the plugin in the menu under **Plugins » Purchases**.

Then you have to activate the plugin in the desired *plugin set* and make it available.

Go to **Settings » Client » Settings » Services » Feed4Ceres Login** and connect your Instagram Account with Plenty. See Video guide!

Now the widget is available in the Shop Builder and can be used via drag & drop. :sunglasses: :rocket:
    
## Further reading
    
* More information you can find here :point_right: [ARMANDO SCHMITT Digital Marketing](https://www.armandoschmitt.de)
    