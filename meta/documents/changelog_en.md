# Release Notes für Feed4Ceres

## v1.1.5 (2019-06-10)

### Fixed

- Debug Message shown at bottom of Feed4Ceres View was deleted

## v1.1.4 (2019-05-22)

### Added

- Compatibility with Ceres 3 and 4

## v1.1.3 (2019-05-08)

### Added

- New Contact Info for Author and Company
- Logo and Media has been updated

## v1.1.2 (2019-03-28)

### Added

- Compatibility with Ceres 3.2

### Fixed

- Image size behaviour on Grid was fixed.

## v1.1.1 (2019-03-26)

### Fixed

- Console messages were deleted.

## v1.1.0 (2019-03-12)

### Added

- Video posts are labelled with an special icon.
- Video posts can now be played in Modal.
- Carousel posts are labelled with an special icon.
- Carousel posts can be viewed in Modal.
- Total support for Carousel Media. 
- Compatibility with Ceres 3

## v1.0.5 (2019-02-24)

### FIXED

- A problem related with size on mobiles was solved.

## v1.0.4 (2019-02-21)

### FIXED

- Managing of filter when Widget is used as a PlugIn
- Modal pagination won't be indexable.

### Added
- Enable filter composed. Each word must be separated by space. More than one word means an OR connector for words
- Enable filter composed when Widget is used as a PlugIn
- Enable bootstrap button types for More Media Button using Configuration Panel
- Enable text for More Media button using Configuration Panel

## v1.0.3 (2019-01-25)

### FIXED
- Fixing managing next and previous Icon on Modal
- Fixing Modal Buttons
- Fixing some Buttons
- Error Reported when some characters appear on Text Post.

### Added
- Code Cleaning

### Deleted
- Commented Code

## v1.0.1 (2019-01-09)
    
### FIXED
- Fix Modal styles affecting Ceres Modals
- Fix span onclick error on ShopBuilder


## v1.0.0 (2019-01-02)
    
### Added
- New Feed4Ceres-Widget
- New Settings for the global configuration and styles
- New Route for the connection of the Instagram account