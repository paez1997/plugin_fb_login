# Feed4Ceres-Widget für Shop-Builder
    
Dieses Plugin fügt eine Sektion mit dem Feed (Media Content) Ihres Instagram Accounts zum Shop-Builder hinzu. Die Eigenschaften der globale Elemente lassen sich einzeln einstellen. Ebenfalls ist es möglich anhand eines Hashtags # nach Bildern Filtern, die diesen Hastag beinhalten. Somit werden z.B. nur Bilder gezeigt, die z.B. #rot beinhalten.

## Beispiel Einrichtung

<iframe src="https://spark.adobe.com/video/2ojgRe0hyYfqk"  width="100%" height="512" frameborder="0" allowfullscreen></iframe>
    
## Anforderungen
<div class="alert alert-info" role="alert"> 
:rotating_light: Sie benötigen einen installierten Ceres-Shop sowie das IO Plugin als Basis. Außerdem muss der Shop Builder aktiviert sein. :bangbang:
</div>

## Installation
    
Laden Sie das Plugin-Widget aus dem plentyMarketplace herunter. In Ihrem Backend von Plentymarkets können Sie das Plugin im Menü unter **Plugins » Einkäufe** installieren. 

Anschließend müssen Sie das Plugin in dem gewünschten Plugin Set aktivieren und bereitstellen.

Öffnen Sie das Menü **System » Mandant » Einstellungen » Dienste » Feed4Ceres Login**

Anschliessend Loggen Sie sich mit Ihrem Instagram Account ein.

Sobald eine erfolgreiche Bestätigung angezeigt wird können Sie fortfahren.

Jetzt ist das Widget im Shop-Builder verfügbar und kann per Drag & Drop eingesetzt werden. :sunglasses: :rocket:
    
## Weitere Informationen
    
* Mehr Informationen finden Sie :point_right: [ARMANDO SCHMITT Digital Marketing](https://www.armandoschmitt.de)
    