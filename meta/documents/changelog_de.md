# Release Notes für Feed4Ceres

## v1.1.5 (2019-06-10)

### Fixed

- Die unten in der Feed4Ceres-Ansicht angezeigte Debug-Meldung wurde gelöscht.

## v1.1.4 (2019-05-22)

### Neu

- Kompatibilität mit Ceres 3, 4

## v1.1.3 (2019-05-08)

### Neu

- Neue Kontaktdaten und Author Umbennenung.
- Die Logos für Author wurden aktualisiert.

## v1.1.2 (2019-03-28)

### Neu

- Kompatibilität mit Ceres 3.2

### Fixed

- Das Bildgrößenverhalten im Raster wurde korrigiert.

## v1.1.1 (2019-03-26)

### Fixed

- Konsolenmeldungen wurden gelöscht.

## v1.1.0 (2019-03-12)

### Neu

- Videobeiträge sind mit einem speziellen Symbol gekennzeichnet.
- Videobeiträge können nun in Modal abgespielt werden.
- Karussellpfeilen sind mit einem speziellen Symbol gekennzeichnet.
- Karussellposts können in Modal angesehen werden.
- Vollständiger Support für Carousel Media. 
- Kompatibilität mit Ceres 3

## v1.0.5 (2019-02-24)

### FIXED

- Problem mit der Größe der Bilder auf Mobile-Devices wurde behoben.

## v1.0.4 (2019-02-21)

### FIXED

- Verwaltung des Filters bei Verwendung des Widgets als PlugIn
- Modale Paginierung wird nicht mehr indexiert.

### Hinzugefügt
- Aktivierung der Filterfunktion. Jedes Wort muss durch ein Leerzeichen getrennt werden. Mehr als ein Wort bedeutet ein ODER-Anschluss für Wörter.
- Aktivieren der Filterfunktion, wenn das Widget als PlugIn über einen Container-Link verwendet wird.
- Neuer Button auf Bootstrap-Basis. Konfiguration über Panel.
- Text für See More... Schaltfläche lässt sich konfigurieren.

## v1.0.3 (2019-01-25)

### FIXED
- Fixing für Weiter und Zurück Pfeile Modal
- Fixing Modal Buttons
- Gemeldeter Fehler: Bei manchen Posts mit @ oder Sonderzeichen wurde der Text nicht gezeigt.

### Added
- Code Cleaning

### Deleted
- Kommentare im Code

## v1.0.1 (2019-01-09)
    
### FIX
- Einige Modal Styles, die mit den Ceres Modals kollidiert sind
- Fix span onclick error für ShopBuilder

## v1.0.0 (2019-01-02)
    
### Hinzugefügt
- Neues Feed4Ceres-Widget
- Neue Settings für die globale Konfiguration und das Design
- Neue Settings für die Anzahl an Spalten für die verschiedene Devices
- Neue Route um die Verknüpfung des Instagram-Accounts einzurichten