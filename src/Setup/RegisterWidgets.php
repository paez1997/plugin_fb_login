<?php

namespace Feed4Ceres\Setup;

use IO\Services\WebstoreConfigurationService;
use Plenty\Modules\Plugin\Models\Plugin;
use Plenty\Modules\Plugin\Contracts\ConfigurationRepositoryContract;
use Plenty\Modules\Plugin\Contracts\PluginRepositoryContract;
use Plenty\Modules\ShopBuilder\Contracts\ContentLinkRepositoryContract;
use Plenty\Modules\ShopBuilder\Contracts\ContentRepositoryContract;
use Plenty\Modules\System\Module\Contracts\PlentyModuleRepositoryContract;
class RegisterWidgets
{

    public function run()
    {
        $pluginSetId = pluginSetId();

        $contentRepository = pluginApp( ContentRepositoryContract::class );

        $contentLinkRepository = pluginApp( ContentLinkRepositoryContract::class );

        $contentLinks = $contentLinkRepository->getContentLinks( $pluginSetId );
        foreach( $contentLinks as $contentLink )
        {
            $content = $contentRepository->getContent( $contentLink->contentId );
            $contentRepository->updateContent(
                $pluginSetId,
                $content->id,
                [
                    'widgets' => $content->widgets
                ]
            );
        }
    }
}