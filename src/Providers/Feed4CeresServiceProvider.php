<?php

namespace Feed4Ceres\Providers;


use Plenty\Plugin\ServiceProvider;
use IO\Helper\ResourceContainer;
use Plenty\Plugin\Events\Dispatcher;
use Plenty\Plugin\Templates\Twig;

class Feed4CeresServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->getApplication()->register(Feed4CeresRouteServiceProvider::class);
    }

    /**
     * Boot a template for the basket that will be displayed in the template plugin instead of the original basket.
     */
    public function boot(Twig $twig, Dispatcher $eventDispatcher)
    {
        $eventDispatcher->listen('IO.Resources.Import', function (ResourceContainer $container)
        {
            // The style is imported in the <head> on the PageDesign.twig of Ceres
            $container->addScriptTemplate('Feed4Ceres::content.Feed4CeresScript');
            $container->addStyleTemplate('Feed4Ceres::content.Feed4CeresStyle');
        }, 0);
    }
}
