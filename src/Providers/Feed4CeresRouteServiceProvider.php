<?php

namespace Feed4Ceres\Providers;


use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\Router;

class Feed4CeresRouteServiceProvider extends RouteServiceProvider
{
    public function map(Router $router)
    {
        $router->post('feed4ceres', 'Feed4Ceres\Containers\Feed4CeresContainer@call');
        $router->get('feed4ceres', 'Feed4Ceres\Containers\Feed4CeresContainer@callGET');
    }
}
