<?php
/**
 * Created by PhpStorm.
 * User: nsierrar
 * Date: 10/9/18
 * Time: 8:24 PM
 */

namespace Feed4Ceres\Helpers;


class Feed4CeresUtil
{
    function pixelpunkt_instagram_api_curl_connect( $api_url ){
        $connection_c = curl_init(); // initializing
        curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
        curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print
        curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
        $json_return = curl_exec( $connection_c ); // connect and get json data
        curl_close( $connection_c ); // close connection
        return json_decode( $json_return, true ); // decode and return
    }

    function array_to_string($array_tags){
        return(implode("",$array_tags));
    }

    function string_to_array($string_tags){
        return(explode(" ",$string_tags));
    }

    function print_array($one_array){
        foreach($one_array as $item){
            echo $item.'<br/>';
        }
    }

    // Search TAGS with # and return it as an array

    function recover_all_tags($input){
        $regex = '~#\w+~';

        if (preg_match_all($regex, $input, $matches, PREG_PATTERN_ORDER)) {
            return $matches[0];
        }else{
            return null;
        }
    }

    function pixel_match($needles, $haystack)
    {
        foreach($needles as $needle){
            if (strpos(strtolower($haystack), strtolower($needle)) !== false) {
                return true;
            }
        }
        return false;
    }

    function getPropertyValue($listProperties,$nameField){
        foreach($listProperties as $property){
            $valueP = $property['name'];
            if (strcmp($valueP, $nameField)==0) {
                return $property['description'];
            }
        }
        return "";
    }

}