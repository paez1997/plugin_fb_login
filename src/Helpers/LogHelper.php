<?php

namespace Feed4Ceres\Helpers;

use Plenty\Plugin\Log\Loggable;

class LogHelper
{
    use Loggable;

    public function getLog($identifier)
    {
        return $this->getLogger($identifier);
    }
}
