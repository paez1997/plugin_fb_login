<?php

namespace Feed4Ceres\Helpers;

use IO\Helper\PluginConfig;
use Plenty\Plugin\ConfigRepository;

class Feed4CeresConfig extends PluginConfig
{

    public $config;
    public $response;
    public $userInst4Ceres;
    public $pwdInst4Ceres;
    public $tokenInst4Ceres;
    public $numberOfMediaToRecover;
    public $numberOfMediaToShow;
    public $listOfTAGsToSearch;
    public $arrayOfTAGsToSearch;
    public $valueNumberOfMedia;
    public $valueNumberToShow;
    public $isThereTAGs;
    public $url_instaUsingTAGs;
    public $businessID;
    public $mainTitle;
    public $submainTitle;


    public function __construct(ConfigRepository $configRepository)
    {

        $this->userInst4Ceres = $configRepository->get("Feed4Ceres.userInst4Ceres.text");
        $this->pwdInst4Ceres = $configRepository->get("Feed4Ceres.passwordInst4Ceres.text");
        $this->tokenInst4Ceres = $configRepository->get("Feed4Ceres.accessToken.text");
        $this->numberOfMediaToRecover = $configRepository->get("Feed4Ceres.mediaToRecover.text");
        $this->listOfTAGsToSearch = $configRepository->get("Feed4Ceres.filterTAGS.text");
        $this->arrayOfTAGsToSearch = $configRepository->get("Feed4Ceres.filterTAGS.text");
        $this->numberOfMediaToShow = $configRepository->get("Feed4Ceres.mediaToShowOn.text");
        $this->businessID = $configRepository->get("Feed4Ceres.instagrambi.text");
        $this->mainTitle = $configRepository->get("Feed4Ceres.mainTitle.text");
        $this->submainTitle = $configRepository->get("Feed4Ceres.submainTitle.text");



        if($this->numberOfMediaToRecover != ''){
            $this->valueNumberOfMedia = intval($this->numberOfMediaToRecover);
        }else{
            $this->valueNumberOfMedia = 20;
        }

        if($this->numberOfMediaToShow != ''){
            $this->valueNumberToShow = intval($this->numberOfMediaToShow);
        }else{
            $this->valueNumberToShow = 6;
        }

        if($this->listOfTAGsToSearch != ''){
            $this->isThereTAGs = true;
            $this->arrayOfTAGsToSearch = explode(',', $this->listOfTAGsToSearch);
        }else{
            $this->isThereTAGs = false;
        }
    }
}