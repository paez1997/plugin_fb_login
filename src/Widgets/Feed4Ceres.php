<?php

namespace Feed4Ceres\Widgets;

use Feed4Ceres\Widgets\Helper\BaseWidget;

class Feed4Ceres extends BaseWidget
{
    protected $template = "Feed4Ceres::Widgets.Feed4Ceres";

    protected function getTemplateData($widgetSettings, $isPreview)
    {

        return ["isPreview" => $isPreview];
    }
}
