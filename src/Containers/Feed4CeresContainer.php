<?php

namespace Feed4Ceres\Containers;

use Feed4Ceres\Helpers\Feed4CeresConfig;
use Feed4Ceres\Helpers\Feed4CeresUtil;
use Plenty\Plugin\Templates\Twig;
use Plenty\Modules\Item\DataLayer\Contracts\ItemDataLayerRepositoryContract;
use Plenty\Plugin\Http\Response;
use Plenty\Plugin\ConfigRepository;
use Plenty\Plugin\Http\Request;
use Plenty\Modules\Authorization\Services\AuthHelper;
use Plenty\Modules\Property\Contracts\PropertyNameRepositoryContract;
use Feed4Ceres\Helpers\LogHelper;


class Feed4CeresContainer
{
    /*
     *
     * public function showInst4Ceres(Twig $twig, ItemDataLayerRepositoryContract $itemRepository, Request $request)
     */

    public static $mediaLoaded = [];

    function __construct(){
        $log = pluginApp(LogHelper::class);
        $log->getLog("Feed4Ceres")->info("", ["Constructor size of mediaLoaded: " => count(self::$mediaLoaded)]);
    }

    function loadMedia($numberOfPosts, $urlInsta, $filterToApplyOriginal){

        $util = pluginApp(Feed4CeresUtil::class);

        $filterToApply = str_replace(array(",", ";", "#"), " ", $filterToApplyOriginal);

        $mtLoad = $numberOfPosts;
        $dataLoaded = 0;
        $second = 0;
        $dataExist = 1;

        $tempoLoad = $mtLoad;
        $nextURL = '';
        $indexi = 0;

        $withFilter=0;

        if($filterToApply != '' and $filterToApply != null){
            $filter_as_array = $util->string_to_array($filterToApply);
            $withFilter=1;
        }

        self::$mediaLoaded = [];

        while($dataLoaded < $mtLoad and $dataExist == 1){
            if($second == 0){
                $rec1 = $util->pixelpunkt_instagram_api_curl_connect($urlInsta);
                $nextURL = $rec1['pagination']['next_url'];

                $indexi = 0;
                foreach ($rec1['data'] as $itemInsta) {
                    //echo '<br>'.$itemInsta['images']['standard_resolution']['url'];
                    $textPost = str_replace(array("\r\n", "\r", "\n", "\\r", "\\n", "\\r\\n"), "<br/>", $itemInsta['caption']['text']);
                    $tags = $util->array_to_string($util->recover_all_tags($textPost));
                    if($withFilter==1){
                        if ($util->pixel_match($filter_as_array, $tags)) {
                            //echo '<br>Added with filter';
                            self::$mediaLoaded[] = $itemInsta;
                            $indexi++;
                            if ($indexi >= $mtLoad) {
                                break;
                            }
                        }
                    }else{
                        //echo '<br>Added without filter';
                        self::$mediaLoaded[] = $itemInsta;
                        $indexi++;
                        if ($indexi >= $mtLoad) {
                            break;
                        }
                    }
                }

                $dataLoaded = count(self::$mediaLoaded);
                //echo '<br>Added '.$dataLoaded.' records';
                if($nextURL != null and $nextURL != '') {
                    $dataExist = 1;
                }else{
                    $dataExist = 0;
                }
                $second = 1;

            }else{
                $rec1 = $util->pixelpunkt_instagram_api_curl_connect($nextURL);
                $nextURL = $rec1['pagination']['next_url'];
                foreach ($rec1['data'] as $itemInsta) {
                    //echo '<br>'.$itemInsta['images']['standard_resolution']['url'];
                    $textPost = str_replace(array("\r\n", "\r", "\n", "\\r", "\\n", "\\r\\n"), "<br/>", $itemInsta['caption']['text']);
                    $tags = $util->array_to_string($util->recover_all_tags($textPost));
                    if($withFilter==1){
                        if ($util->pixel_match($filter_as_array, $tags)) {
                            //echo '<br>Added with filter';
                            self::$mediaLoaded[] = $itemInsta;
                            $indexi++;
                            if ($indexi >= $mtLoad) {
                                break;
                            }
                        }
                    }else{
                        //echo '<br>Added without filter';
                        self::$mediaLoaded[] = $itemInsta;
                        $indexi++;
                        if ($indexi >= $mtLoad) {
                            break;
                        }
                    }
                }

                $dataLoaded = count(self::$mediaLoaded);
                //echo '<br>Added '.$dataLoaded.' records';
                if($nextURL != null and $nextURL != '') {
                    $dataExist = 1;
                }else{
                    $dataExist = 0;
                }
            }
        }
    }


    public function call(Twig $twig, ItemDataLayerRepositoryContract $itemRepository, Request $request): string
    {

        $dataInsta = array();
        $dataCarousel = array();

        $indexGeneralCarousel = 0;
        $indexInitialCarousel = 0;
        $indexFinalCarousel = 0;

        $urlGet = '';
        $recoverMedia = 0;

        $property = pluginApp(PropertyNameRepositoryContract::class);
        $authHelper = pluginApp(AuthHelper::class);
        $log = pluginApp(LogHelper::class);


        $sekret = $request->get("hidden"); //Current page number

        $listProperties = null;

        $listProperties = $authHelper->processUnguarded(
            function () use ($property, $listProperties) {
                //unguarded
                return $property->listNames();
            }
        );

        $filterConfig = '';

        $config = pluginApp(Feed4CeresConfig::class);

        $util = pluginApp(Feed4CeresUtil::class);


        $numberOfPage = 0;

        $access_token = ""; // Access Token to use FB API Graph


        // Access Token for Instagram API

        $instaAccessToken = '';

        // Set constant for testing
        // Davo, recover Instagram token and assign it to variable $instaAccessToken

        //$instaAccessToken='8180472080.f08bec3.f8f56f9d13284e168d57a3f6489fb3b1';

        $instaAccessToken = $util->getPropertyValue($listProperties, 'instagram_token');
        $instagrambi = $util->getPropertyValue($listProperties, 'instagram_id');

        $log->getLog("Feed4Ceres")->info("", ["instaAccessToken" => $instaAccessToken]);

        $log->getLog("Feed4Ceres")->info("", ["Sekret: " => $sekret]);


        $paramvalue = $request->get("pagenumber"); //Current page number
        $commandnop = $request->get("command"); //Command: m

        $log->getLog("Feed4Ceres")->info("", ["pagenumber" => $paramvalue]);
        $log->getLog("Feed4Ceres")->info("", ["command" => $commandnop]);

        $pMT = $request->get("mt");
        $log->getLog("Feed4Ceres")->info("", ["mt" => $pMT]);
        $withExternalMT = 0;

        $pST = $request->get("sm");
        $log->getLog("Feed4Ceres")->info("", ["sm" => $pST]);
        $withExternalST = 0;

        $pMTR = $request->get("mtr");
        $log->getLog("Feed4Ceres")->info("", ["mtr" => $pMTR]);
        $withExternalMTR = 0;

        $pMTS = $request->get("mts");
        $log->getLog("Feed4Ceres")->info("", ["mts" => $pMTS]);
        $withExternalMTS = 0;

        $pFT = $request->get("ftg");
        $log->getLog("Feed4Ceres")->info("", ["ftg" => $pFT]);


        if ($pMT != null && $pMT != '') {
            $withExternalMT = 1;
        } else {
            $withExternalMT = 0;
        }

        if ($pST != null && $pST != '') {
            $withExternalST = 1;
        } else {
            $withExternalST = 0;
        }

        if ($pMTR != null && $pMTR != '') {
            $withExternalMTR = 1;
        } else {
            $withExternalMTR = 0;
        }

        if ($pMTS != null && $pMTS != '') {
            $withExternalMTS = 1;
        } else {
            $withExternalMTS = 0;
        }

        $withExternalFilter = 0;

        $filterTAGS = $pFT;

        if ($filterTAGS != null && $filterTAGS != '') {
            $withExternalFilter = 1;
        }

        if ($paramvalue != null && $paramvalue != '') {
            if ($commandnop == 'm') {
                $numberOfPage = intval($paramvalue) + 1;
                $recoverMedia = 0;
            }
        } else {
            $numberOfPage = 0;
            $recoverMedia = 1;
        }

        if ($withExternalMTS == 0) {
            $numberToShow = $config->valueNumberToShow;
        } else {
            $numberToShow = intval($pMTS);
            $urlGet .= '&mts=' . $pMTS;
        }

        if ($withExternalMTR == 0) {
            $numberOfMedia = $config->valueNumberOfMedia;
        } else {
            $numberOfMedia = intval($pMTR);
            $urlGet .= '&mtr=' . $pMTR;
        }

        if ($withExternalMT == 0) {
            $mainTitle = $config->mainTitle;
        } else {
            $mainTitle = urldecode($pMT);
            $log->getLog("Feed4Ceres")->info("", ["mainTitle decoded" => $mainTitle]);
            $urlGet .= '&mt=' . rawurlencode($pMT);
        }

        if ($withExternalST == 0) {
            $submainTitle = $config->submainTitle;
        } else {
            $submainTitle = urldecode($pST);
            $log->getLog("Feed4Ceres")->info("", ["submainTitle decoded" => $submainTitle]);
            $urlGet .= '&sm=' . rawurlencode($pST);
        }

        $initialMedia = 0; // Always begins in 0 (First picture)

        // Control variables

        //$url_insta = "https://api.instagram.com/v1/users/self/media/recent/?access_token=" . $access_token . "&count=" . $config->valueNumberOfMedia;

        //$url_insta='https://api.instagram.com/v1/users/self/media/recent/?access_token='.$instaAccessToken.'&count='.$config->valueNumberOfMedia;

        //$url_insta = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $instaAccessToken . '&count=' . $numberOfMedia;

        //$instaAccessToken = '613331471.968916a.c859ba8b6b3349b19d29d5343d1f6cb3';

        $url_insta = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $instaAccessToken . '&count=33';

        //$url_insta = 'https://graph.facebook.com/'.$instagrambi.'/media?limit='.$numberOfMedia.'&fields=caption,children,comments,comments_count,id,ig_id,is_comment_enabled,like_count,media_type,media_url,owner,permalink,shortcode,thumbnail_url,timestamp,username&access_token='.$access_token;

        $url_accountinsta = 'https://graph.facebook.com/' . $instagrambi . '?fields=profile_picture_url,name,username,website&access_token=' . $access_token;


        //if($config->isThereTAGs) {
        //    $config->url_instaUsingTAGs = "https://api.instagram.com/v1/tags/" . $config->listOfTAGsToSearch . "/media/recent?access_token=" . $access_token . "&count=" . $config->valueNumberOfMedia;
        //}

        $withFilter = 0;
        $filterToApply = '';

        $log->getLog("Feed4Ceres")->info("", ["Filter on Config" => $config->listOfTAGsToSearch]);


        if ($config->isThereTAGs) {
            $log->getLog("Feed4Ceres")->info("", ["isThereTAGs" => 'Si hay TAGS en PlugIn']);
            $filterConfig = $config->listOfTAGsToSearch;
        }

        if ($config->isThereTAGs) {
            $log->getLog("Feed4Ceres")->info("", ["isThereTAGs" => 'Si hay TAGS en PlugIn']);
            $filterToApply = $filterConfig;
            $withFilter = 1;
        }

        if ($withExternalFilter == 1) {
            $filterToApply = urldecode($filterTAGS);
            $urlGet .= '&ftg=' . rawurlencode($filterTAGS);
            $withFilter = 1;
        }

        $log->getLog("Feed4Ceres")->info("", ["filterToApply" => $filterToApply]);

        $log->getLog("Feed4Ceres")->info("", ["urlGET" => $urlGet]);

        // Load media in self::$mediaLoaded

        //if($recoverMedia == 1){
            self::loadMedia($numberOfMedia, $url_insta, $filterToApply);
        //}

        $item_insta = self::$mediaLoaded[0];

        if ($item_insta != null) {
            $mainID = $item_insta['id'];
            $userName = $item_insta['user']['username'];
            $userId = $item_insta['user']['id'];
            $profilePicture = $item_insta['user']['profile_picture'];
            $fullName = $item_insta['user']['full_name'];

            //$userInsta[] = array('mainID' => $mainID, 'userName' => $userName, 'userId' => $userId, 'profilePicture' => $profilePicture, 'fullName' => $fullName, 'numberToRecover' => $config->valueNumberOfMedia, 'filterTAGs' => $config->listOfTAGsToSearch[0], 'numberMedia' => $numberOfMedia, 'pagenumber' => $numberOfPage, 'ini' => $initialMedia, 'end' => $finalMedia, 'more' => $moreButton);

        } else {
            $mainID = '';
            $userName = '';
            $userId = '';
            $profilePicture = '';
            $fullName = '';

            //$userInsta[] = array('mainID' => $mainID, 'userName' => $userName, 'userId' => $userId, 'profilePicture' => $profilePicture, 'fullName' => $fullName, 'numberToRecover' => $config->valueNumberOfMedia, 'filterTAGs' => $config->listOfTAGsToSearch[0], 'numberMedia' => $numberOfMedia, 'pagenumber' => $numberOfPage, 'ini' => $initialMedia, 'end' => $finalMedia, 'more' => $moreButton);

        }

        $numberOfMediaLoaded = count(self::$mediaLoaded);

        $log->getLog("Feed4Ceres")->info("", ["numberOfMediaLoaded" => $numberOfMediaLoaded]);


        $dataInstaFull = array();
        $userInsta = array();

        $dataCarousel = array();
        $dataCarouselWidths = array();
        $dataCarouselHeights = array();

        $carGeneralIndex = 0;
        $carInitial = 0;
        $carFinal = 0;


        for ($i = 0; $i < $numberOfMediaLoaded; ++$i) {

            $item_insta = self::$mediaLoaded[$i];

            $imgMedia = $item_insta['images']['standard_resolution']['url'];
            $imgMediaWidth = $item_insta['images']['standard_resolution']['width'];
            $imgMediaHeight = $item_insta['images']['standard_resolution']['height'];

            $createdTime = $item_insta['created_time'];
            $textPost = str_replace(array("\r\n", "\r", "\n", "\\r", "\\n", "\\r\\n"), "<br/>", $item_insta['caption']['text']);
            $tempPost = utf8_encode($textPost);

            if (strlen($textPost) > 235) {
                $posi = stripos($tempPost, ' ', 230);
                $textPostResume = substr($textPost, 0, $posi) . ' ...';
            } else {
                $textPostResume = $textPost;
            }
            $countLikes = $item_insta['likes']['count'];
            $tags = $item_insta['tags'];
            $countComments = $item_insta['comments']['count'];
            $typeMedia = $item_insta['type'];
            $mediaVideo = '';
            $videoW = '';
            $videoH = '';
            if (strcmp($typeMedia, "video") == 0) {
                $mediaVideo = $item_insta['videos']['standard_resolution']['url'];
                $videoW = $item_insta['videos']['standard_resolution']['width'];
                $videoH = $item_insta['videos']['standard_resolution']['height'];
            }

            if (strcmp($typeMedia, "carousel") == 0) {
                $mediaCar = $item_insta['carousel_media'];
                $numberCarouselImages = count($mediaCar);
                $carInitial = $carGeneralIndex;
                $carFinal = $carGeneralIndex;
                foreach ($mediaCar as $itemCar) {
                    $dataCarousel[] = array('imgcarou' => $itemCar['images']['standard_resolution']['url']);
                    $carFinal ++;
                    $carGeneralIndex ++;
                }

                if($carFinal > 0){
                    $carFinal --;
                }
            }

            $linkInstagram = $item_insta['link'];
            if ($item_insta['location'] != null) {
                $locationName = $item_insta['location']['name'];
            } else {
                $locationName = '';
            }

            // We are trying, This line was commented because we are using tags field returned by Instagram API
            $tags = $util->array_to_string($util->recover_all_tags($textPost));


            $dataInstaFull[] = array('videoMedia' => $mediaVideo, 'videoW' => $videoW, 'videoH' => $videoH, 'imgMedia' => $imgMedia, 'imageWidth' => $imgMediaWidth, 'imageHeight' => $imgMediaHeight, 'imgThumbnail' => $imgMedia, 'createdTime' => $createdTime, 'textPost' => $textPost, 'countLikes' => $countLikes, 'tags' => $tags, 'countComments' => $countComments, 'typeMedia' => $typeMedia, 'linkInstagram' => $linkInstagram, 'locationName' => $locationName, 'textPostResume' => $textPostResume, 'carInitial' => $carInitial, 'carFinal' => $carFinal);

        }

        $numberMediaToShow = count($dataInstaFull);

        if ((($numberOfPage + 1) * $numberToShow) > $numberMediaToShow) {
            $finalMedia = $numberMediaToShow;
        } else {
            $finalMedia = (($numberOfPage + 1) * $numberToShow);
        }

        $log->getLog("Feed4Ceres")->info("", ["initialMedia" => $initialMedia]);
        $log->getLog("Feed4Ceres")->info("", ["finalMedia" => $finalMedia]);
        $log->getLog("Feed4Ceres")->info("", ["numberOfMediaLoaded filtered" => $numberMediaToShow]);


        $moreButton = 'visibility: hidden;';

        if ($finalMedia < $numberMediaToShow) {
            $moreButton = 'visibility: visible;';
        }

        $userInsta[] = array('userName' => $userName, 'userId' => $userId, 'profilePicture' => $profilePicture, 'fullName' => $fullName, 'numberToRecover' => $config->valueNumberOfMedia, 'filterTAGs' => $config->listOfTAGsToSearch, 'numberMedia' => $numberOfMedia, 'pagenumber' => $numberOfPage, 'ini' => $initialMedia, 'end' => $finalMedia, 'more' => $moreButton, 'ibi' => $instagrambi, 'at' => $access_token, 'urlGet' => $urlGet, "MT" => $mainTitle, "SMT" => $submainTitle, "MTR" => $pMTR, "MTS" => $pMTS, "FT" => $filterToApply);


        for ($i = $initialMedia; $i < $finalMedia; ++$i) {

            $item_insta = $dataInstaFull[$i];

            $dataInsta[] = $item_insta;
        }

        $templateData = array(
            'dataUser' => $userInsta,
            'currentItems' => $dataInsta,
            'dataCarousel' => $dataCarousel,
            'instavalid' => 1
        );

        $log->getLog("Feed4Ceres")->info("", ["Render Feed4Ceres: " => $sekret]);
        $log->getLog("Feed4Ceres")->info("", ["Show Feed4Ceres: " => 'Request valid']);


        return $twig->render('Feed4Ceres::content.Feed4Ceres', $templateData);

    }

    public function callGET(Twig $twig, ItemDataLayerRepositoryContract $itemRepository, Request $request): string
    {
        $log = pluginApp(LogHelper::class);

        $log->getLog("Feed4Ceres")->info("", ["Render Homepage: " => 'Because GET Request']);

        $templateData = array(
            'dataUser' => array(),
            'currentItems' => array(),
            'instavalid' => 0
        );

        $log->getLog("Feed4Ceres")->info("", ["Show Feed4Ceres: " => 'Request Invalid']);

        return $twig->render('Feed4Ceres::content.Feed4Ceres', $templateData);
    }
}